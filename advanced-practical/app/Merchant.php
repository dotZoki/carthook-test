<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'merchant_id',
        'first_name',
        'last_name'
    ];
}
