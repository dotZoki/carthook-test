<?php


namespace App\Console\Commands\DataImport\Descriptors;


use Carbon\Carbon;

final class MerchantDailyDescriptor extends Descriptor
{

    private static $validatorRules = [
        'merchant_id'                => 'required|string|max:12',
        'analytics_date'             => 'required|date',
        'sales_total'                => 'required',
        'conversions_total'          => 'required|integer',
        'postpurchase_revenue_total' => 'required',
        'visits_total'               => 'required|integer',
        'ident'                      => 'required|string|max:30',
    ];

    public static $sourceUri = 'https://app.periscopedata.com/api/carthook/chart/csv/d5345630-811c-cd5a-b3e2-c4a6ac1dae1a/265769';

    public static function getSourceUri()
    {
        return self::$sourceUri;
    }

    public static function getSaveAs()
    {
        return sprintf('import/merchant_daily/%s_%s', Carbon::now('UTC')->toIso8601ZuluString(), md5(self::$sourceUri));
    }

    public static function getValidatorRules()
    {
        return static::$validatorRules;
    }

    public static function getTableName()
    {
        return 'merchant_daily_analytics';
    }
}
