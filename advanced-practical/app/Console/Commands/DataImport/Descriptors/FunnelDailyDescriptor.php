<?php


namespace App\Console\Commands\DataImport\Descriptors;


use Carbon\Carbon;

final class FunnelDailyDescriptor extends Descriptor
{

    private static $validatorRules = [
        'merchant_id'                => 'required|string|max:12',
        'funnel_id'                => 'required|integer',
        'analytics_date'             => 'required|date',
        'sales_total'                => 'required',
        'conversions_total'          => 'required|integer',
        'postpurchase_revenue_total' => 'required',
        'visits_total'               => 'required|integer',
        'ident'                      => 'required|string|max:30',
    ];

    public static $sourceUri = 'https://app.periscopedata.com/api/carthook/chart/csv/b3bb3bbd-0ea3-8234-64bd-3cb474631c30/284541';

    public static function getSourceUri()
    {
        return self::$sourceUri;
    }

    public static function getSaveAs()
    {
        return sprintf('import/funnel_daily/%s_%s', Carbon::now('UTC')->toIso8601ZuluString(), md5(self::$sourceUri));
    }

    public static function getValidatorRules()
    {
        return static::$validatorRules;
    }

    public static function getTableName()
    {
        return 'funnel_daily_analytics';
    }
}
