<?php


namespace App\Console\Commands\DataImport\Descriptors;


use Carbon\Carbon;

final class MerchantHourlyDescriptor extends Descriptor
{

    private static $validatorRules = [
        'merchant_id'                => 'required|string|max:12',
        'analytics_date'             => 'required|date',
        'sales_total'                => 'required',
        'conversions_total'          => 'required|integer',
        'postpurchase_revenue_total' => 'required',
        'visits_total'               => 'required|integer',
        'ident'                      => 'required|string|max:50',
    ];

    public static $sourceUri = 'https://app.periscopedata.com/api/carthook/chart/csv/5ab06803-29bf-76b2-6a5a-ad7cf4b7fc21/284541';

    public static function getSourceUri()
    {
        return self::$sourceUri;
    }

    public static function getSaveAs()
    {
        return sprintf('import/merchant_hourly/%s_%s', Carbon::now('UTC')->toIso8601ZuluString(), md5(self::$sourceUri));
    }

    public static function getValidatorRules()
    {
        return static::$validatorRules;
    }

    public static function getTableName()
    {
        return 'merchant_hourly_analytics';
    }
}
