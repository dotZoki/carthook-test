<?php


namespace App\Console\Commands\DataImport\Descriptors;


use Carbon\Carbon;

final class FunnelHourlyDescriptor extends Descriptor
{

    private static $validatorRules = [
        'merchant_id'                => 'required|string|max:12',
        'funnel_id'                => 'required|integer',
        'analytics_date'             => 'required|date',
        'sales_total'                => 'required',
        'conversions_total'          => 'required|integer',
        'postpurchase_revenue_total' => 'required',
        'visits_total'               => 'required|integer',
        'ident'                      => 'required|string|max:50',
    ];

    public static $sourceUri = 'https://app.periscopedata.com/api/carthook/chart/csv/b5798a66-e694-a429-cc5e-2f9e163f6438/284541';

    public static function getSourceUri()
    {
        return self::$sourceUri;
    }

    public static function getSaveAs()
    {
        return sprintf('import/funnel_hourly/%s_%s', Carbon::now('UTC')->toIso8601ZuluString(), md5(self::$sourceUri));
    }

    public static function getValidatorRules()
    {
        return static::$validatorRules;
    }

    public static function getTableName()
    {
        return 'funnel_hourly_analytics';
    }
}
