<?php

namespace App\Console\Commands\DataImport\Descriptors;

abstract class Descriptor
{
    abstract public static function getSourceUri();

    abstract static function getSaveAs();

    abstract public static function getValidatorRules();

    abstract public static function getTableName();

    public static function getSqlFields()
    {
        return array_keys(static::getValidatorRules());
    }

    public static function getFieldsAsSqlParams()
    {
        return array_map(function ($v) {
            return ':' . $v;
        }, self::getSqlFields());
    }
}
