<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class BestSellingMerchant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merchant:best-selling';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Best selling merchant of all the merchants';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bestMerchant = DB::select('
            SELECT
                mda.merchant_id, m.first_name, m.last_name, SUM(mda.sales_total) sales_total
            FROM
                merchant_daily_analytics mda
                    JOIN
                merchants m ON mda.merchant_id = m.merchant_id
            GROUP BY mda.merchant_id
            ORDER BY SUM(mda.sales_total) DESC
            LIMIT 1
        ');
        $headers = ['Merchant id', 'FirstName', 'LastName', 'Total Sales'];

        $this->table($headers, json_decode(json_encode($bestMerchant), true));
    }
}
