<?php

namespace App\Console\Commands;

use App\Console\Commands\DataImport\Descriptors\FunnelDailyDescriptor;
use App\Console\Commands\DataImport\Descriptors\FunnelHourlyDescriptor;
use App\Console\Commands\DataImport\Descriptors\MerchantHourlyDescriptor;
use App\Console\Commands\DataImport\Descriptors\MerchantDailyDescriptor;
use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class DataImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection()->disableQueryLog(); // lowers memory usage
        $dataSourceList = [
            MerchantDailyDescriptor::class,
            FunnelDailyDescriptor::class,
            MerchantHourlyDescriptor::class,
            FunnelHourlyDescriptor::class,
        ];

        foreach ($dataSourceList as $dataSource) {
            $importLocation = $dataSource::getSourceUri();
            $saveAs         = $dataSource::getSaveAs();
            $fullPathOnDisk = Storage::path($saveAs);
            $logFile        = sprintf('%s.log', $saveAs);
            $validatorRules = $dataSource::getValidatorRules();
            $fields         = $dataSource::getSqlFields();
            $params         = $dataSource::getFieldsAsSqlParams();
            $tableName      = $dataSource::getTableName();
            $update         = array_map(function ($v) {
                return sprintf('%s = VALUES(%s)', $v, $v);
            }, $fields);

            $this->info('Working on ' . $dataSource);

            $this->info('Downloading file ' . $importLocation);
            $this->downloadFile($importLocation, $saveAs);

            $this->info('Importing ' . $fullPathOnDisk);

            if (($fHandle = fopen($fullPathOnDisk, 'r')) === false) {
                $this->error('Cannot read ' . $fullPathOnDisk);
                continue;
            }

            $totalFileLines = trim(exec('wc -l <' . $fullPathOnDisk));
            $bar            = $this->output->createProgressBar($totalFileLines);
            $bar->start();

            $header     = $errors = null;
            $lineNumber = 0;
            while (($line = fgetcsv($fHandle)) !== false) {
                if (!isset($header)) {
                    $header = $line;
                    continue;
                }
                ++$lineNumber;
                $line = array_combine($header, $line);

                if (Validator::make($line, $validatorRules)->fails()) {
                    $errorCaught = true;
                    $msg         = sprintf('Validation of read line (#%s) filed', $lineNumber);
                    $this->error($msg);
                    Storage::append($logFile, $msg);
                    continue;
                }
                $sql = sprintf("INSERT INTO %s (%s) VALUES (%s) ON DUPLICATE KEY UPDATE %s",
                    $tableName, implode(',', $fields), implode(', ', $params), implode(', ', $update)
                );

                $this->processLine($line, $sql);
                $bar->advance();
            }
            $bar->finish();
            $this->line('');

            if (isset($errorCaught) && $errorCaught) {
                $this->warn(sprintf('There were some errors for %s. Check %s', $dataSource, $logFile));
            } else {
                $this->info(sprintf('Data imported for %s %s', $dataSource, PHP_EOL));
                Storage::delete($saveAs);
            }

        }
    }

    private function downloadFile($importLocation, $saveAs)
    {
        $fHandle = fopen($importLocation, 'r');
        Storage::put($saveAs, $fHandle);
        fclose($fHandle);
    }

    private function processLine($sql, $line)
    {
        try {
            DB::insert($line, $sql);
        } catch (QueryException $ex) {
            $errorCaught = true;
            $msg         = sprintf('Failed importing line %s, cause: %s', $lineNumber, $ex->getMessage());
            $this->warn($msg);
            Storage::append($logFile, $msg);
        }
    }
}

