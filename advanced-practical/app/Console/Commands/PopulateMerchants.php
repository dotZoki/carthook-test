<?php

namespace App\Console\Commands;

use App\Merchant;
use App\MerchantDailyAnalytics;
use Illuminate\Console\Command;

class PopulateMerchants extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:populate-merchants';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populates merchants';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $faker                 = \Faker\Factory::create();
        $merchantIdsCollection = MerchantDailyAnalytics::distinct('merchant_id')->pluck('merchant_id');

        foreach ($merchantIdsCollection as $merchantId) {
            Merchant::firstOrCreate(['merchant_id' => $merchantId], [
                'first_name' => $faker->firstName(),
                'last_name'  => $faker->lastName
            ]);
        }
    }
}
