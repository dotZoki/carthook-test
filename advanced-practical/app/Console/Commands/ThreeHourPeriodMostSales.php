<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ThreeHourPeriodMostSales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merchant:3-hour-period-most-sales {merchantId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '3 hour time period for specific merchant when he sells most';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $merchantId = $this->argument('merchantId');

    }
}
