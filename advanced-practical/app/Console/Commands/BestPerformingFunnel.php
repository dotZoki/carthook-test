<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class BestPerformingFunnel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'funnel:best-performing {merchantId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'best performing funnel for a specific merchant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $merchantId = $this->argument('merchantId');

        $bestFunnel = DB::select('
            SELECT
                merchant_id, funnel_id, SUM(sales_total) AS sales_total
            FROM
                funnel_daily_analytics
            WHERE
                merchant_id = :merchantId
            GROUP BY merchant_id , funnel_id
            LIMIT 1
        ', ['merchantId' => $merchantId]);

        $headers = ['Merchant id', 'Funnel id', 'Sales Total'];

        $this->table($headers, json_decode(json_encode($bestFunnel), true));
    }
}
