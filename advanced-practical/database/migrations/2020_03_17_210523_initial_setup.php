<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InitialSetup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // merchant_id,analytics_date,sales_total,conversions_total,postpurchase_revenue_total,visits_total,ident
        // mid_06AKI1Jc,2020-03-17,9006.26,59,1236.30,201,mid_06AKI1Jc2020-03-17
        Schema::create('merchant_daily_analytics', function (Blueprint $table) {
            $table->string('merchant_id', 12)->primary();
            $table->date('analytics_date')->index();
            $table->decimal('sales_total', 12, 2);
            $table->integer('conversions_total')->unsigned();
            $table->decimal('postpurchase_revenue_total', 12, 2);
            $table->integer('visits_total')->unsigned();
            $table->string('ident', 30);

            $table->unique(['merchant_id', 'analytics_date']);
        });

        // merchant_id,funnel_id,analytics_date,sales_total,conversions_total,postpurchase_revenue_total,visits_total,ident
        // mid_06AKI1Jc,24878,2020-03-17,4302.48,28,0.00,222,24878mid_06AKI1Jc2020-03-17
        Schema::create('funnel_daily_analytics', function (Blueprint $table) {
            $table->string('merchant_id', 12)->index();
            $table->integer('funnel_id')->index();
            $table->date('analytics_date')->index();
            $table->decimal('sales_total', 12, 2);
            $table->integer('conversions_total')->unsigned();
            $table->decimal('postpurchase_revenue_total', 12, 2);
            $table->integer('visits_total')->unsigned();
            $table->string('ident', 30);

            $table->unique(['merchant_id', 'funnel_id']);
        });
        // merchant_id,analytics_date,sales_total,conversions_total,postpurchase_revenue_total,visits_total,ident
        // mid_06AKI1Jc,2020-03-17 17:00:00,514.30,3,34.30,9,mid_06AKI1Jc2020-03-17 17:00:00

        Schema::create('merchant_hourly_analytics', function (Blueprint $table) {
            $table->string('merchant_id', 12)->primary();
            $table->datetime('analytics_date')->index();
            $table->decimal('sales_total', 12, 2);
            $table->integer('conversions_total')->unsigned();
            $table->decimal('postpurchase_revenue_total', 12, 2);
            $table->integer('visits_total')->unsigned();
            $table->string('ident', 50);

            $table->unique(['merchant_id', 'analytics_date']);
        });

        // merchant_id,funnel_id,analytics_date,sales_total,conversions_total,postpurchase_revenue_total,visits_total,ident
        // mid_06AKI1Jc,32844,2020-03-17 17:00:00,93.00,1,34.30,9,32844mid_06AKI1Jc2020-03-17 17:00:00

        Schema::create('funnel_hourly_analytics', function (Blueprint $table) {
            $table->string('merchant_id', 12)->index();
            $table->integer('funnel_id')->unsigned();
            $table->date('analytics_date')->index();
            $table->decimal('sales_total', 12, 2);
            $table->integer('conversions_total')->unsigned();
            $table->decimal('postpurchase_revenue_total', 12, 2);
            $table->integer('visits_total')->unsigned();
            $table->string('ident', 50);
        });

        Schema::create('merchants', function (Blueprint $table) {
            $table->string('merchant_id', 12)->unique();
            $table->string('first_name', 100);
            $table->string('last_name', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_daily_analytics');
        Schema::dropIfExists('funnel_daily_analytics');
        Schema::dropIfExists('merchant_hourly_analytics');
        Schema::dropIfExists('funnel_hourly_analytics');
        Schema::dropIfExists('merchants');

    }
}
