First I'd like to point out that I'm not happy at all with the structure of the app/solution. I was aiming for more structured
app (repository, service) but because of current situation, not enough time (limited) and lack of my experience with Laravel
I wasn't successful at it. I'm sorry but I also didn't have time for bonus tasks.

For problem A the idea was to have separate job/task which would pull files on certain time period and then another
separate job/task which would parse files or split files into smaller jobs. This is how I thought about solving it.

So I decided to get it working first and make it better later - which I obviously didn't manage :\

I made import command that goes through list of 'DataDescriptors', which contain information about source, validator and some
information needed for storing. After downloading file it is parsed as CSV, but line by line to avoid using too many resources,
With information from DataDescriptor I created UPSERT SQL for each line of CSV and execute it.
Executing each query separately is not really efficient in such amount of queries so this could be optimized by executing multiple queries.

As mentioned before this solution should have been done in separate files/classes, better organized.

There is also option of importing CSV file directly to DB, but that would mean I would have to have file on same server
as DB, which in practice isn't common. Also it is posed as security issue so it is disabled by default.

After running the migrations these are the artisan commands that are available
```
data
  data:import                        Import data
  data:populate-merchants            Populates merchants
funnel
  funnel:best-performing             best performing funnel for a specific merchant
merchant
  merchant:best-selling              Best selling merchant of all the merchants
```