<?php

declare(strict_types=1);

function generateArrayOfRandomNumbers(int $arraySize, int $minNumber, int $maxNumber): array
{
    return array_map(function ($v) use ($minNumber, $maxNumber) {
        $base = rand($minNumber, $maxNumber);
        $exp  = rand($minNumber, $maxNumber);

        return bcpow((string)$base, (string)$exp);
    }, range(0, $arraySize - 1));
}

function mergeSort(array $numbers): array
{
    if (sizeof($numbers) <= 1) {
        return $numbers;
    }
    $splitToSize = (int)ceil(count($numbers) / 2);
    $left        = array_splice($numbers, 0, $splitToSize);

    return _merge(mergeSort($left), mergeSort($numbers));
}

function _merge(array $left, array $right): array
{
    $return = [];

    while (!empty($left) || !empty($right)) {
        if (empty($left) || empty($right)) {
            $return[] = (empty($left))
                ? array_shift($right) : array_shift($left);
        } else {
            $return[] = (bccomp(reset($left), reset($right)) < 0)
                ? array_shift($left) : array_shift($right);
        }
    }

    return $return;
}

$sizesOfRandomArray = [10 ** 1, 10 ** 2, 10 ** 3];
foreach ($sizesOfRandomArray as $size) {
    $arrayToSort = generateArrayOfRandomNumbers($size, 100, 10000);
    $start       = microtime(true);
    mergeSort($arrayToSort);
    $time = microtime(true) - $start;
    printf('Sorted array with %11s elements in %10.6f seconds' . PHP_EOL, $size, $time);
}