<?php

declare(strict_types=1);
ini_set('assert.exception', '1');

function generateArrayOfRandomNumbers(int $arraySize, int $minNumber, int $maxNumber): array
{
    return array_map(function ($v) use ($minNumber, $maxNumber) {
        return rand($minNumber, $maxNumber);
    }, range(0, $arraySize - 1));
}

function insertionSortWithTemp(array $numbers): array
{
    for ($i = 1, $size = count($numbers); $i < $size; $i++) {
        $j = $i;
        while ($j > 0 && $numbers[$j - 1] > $numbers[$j]) {
            $temp            = $numbers[$j];
            $numbers[$j]     = $numbers[$j - 1];
            $numbers[$j - 1] = $temp;
            --$j;
        }
    }

    return $numbers;
}

function insertionSort(array $numbers): array
{
    for ($i = 1, $size = count($numbers); $i < $size; $i++) {
        $compareToValue = $numbers[$i];
        for ($j = $i; ($j > 0) && ($numbers[$j - 1] > $compareToValue); $j--) {
            $numbers[$j] = $numbers[$j - 1];
        }
        $numbers[$j] = $compareToValue;
    }

    return $numbers;
}

$arrayToSort      = generateArrayOfRandomNumbers(11, 0, 100);
$sortedArrayCheck = insertionSort($arrayToSort);

printf('Array to sort: %s' . PHP_EOL, implode(', ', $arrayToSort));

$numOfExecutionsSet = [10 ** 1, 10 ** 2, 10 ** 3, 10 ** 4, 10 ** 5, 10 ** 6, 10 ** 7];
$functions          = ['insertionSort'];
foreach ($functions as $function) {
    foreach ($numOfExecutionsSet as $numOfExecutions) {
        $start = microtime(true);
        for ($i = 0; $i < $numOfExecutions; $i++) {
            $s = $function($arrayToSort);
        }
        $time = microtime(true) - $start;

        printf('Executed %11s calls in %10.6f seconds' . PHP_EOL, $numOfExecutions, $time);
        assert($s === $sortedArrayCheck);
    }

    $numOfExecutions = $numOfExecutions * 1000;
    $time            = $time * 1000 / (60 * 60);
    printf('Executed %11s calls in %10.2f hours [PREDICTION]' . PHP_EOL, $numOfExecutions, $time);
}

printf('Memory usage peak: %.2f MB' . PHP_EOL, memory_get_peak_usage() / (1024 ** 2));