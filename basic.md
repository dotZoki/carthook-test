# CartHook Test Assignment
## Basic CS

Solve exercises A,B,C,D and explain your reasoning. It’s ok to use pseudo code where needed. Or any popular language
you’re comfortable with (except if noted otherwise).

### A) Design a SQL database to store NBA players, teams and games (column and table contents are all up to you). Users mostly query game results by date and team name. The second most frequent query is players statistics by player name.

![ER DIAGRAM](dbdiag.png)

```SQL
-- -----------------------------------------------------
-- Table `mydb`.`teams`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`teams` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`players`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`players` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `current_team_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_current_team_idx` (`current_team_id` ASC),
  CONSTRAINT `FK_current_team`
    FOREIGN KEY (`current_team_id`)
    REFERENCES `mydb`.`teams` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`games`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`games` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `home_team_id` INT UNSIGNED NOT NULL,
  `away_team_id` INT UNSIGNED NOT NULL,
  `game_date` DATE NOT NULL,
  `home_team_score` TINYINT UNSIGNED NOT NULL,
  `away_team_score` TINYINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_home_team_id_idx` (`home_team_id` ASC),
  INDEX `FK_away_team_id_idx` (`away_team_id` ASC),
  INDEX `game_date_idx` (`game_date` DESC),
  CONSTRAINT `FK_home_team_id`
    FOREIGN KEY (`home_team_id`)
    REFERENCES `mydb`.`teams` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_away_team_id`
    FOREIGN KEY (`away_team_id`)
    REFERENCES `mydb`.`teams` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`player_game_stats`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`player_game_stats` (
  `player_id` INT UNSIGNED NOT NULL,
  `game_id` INT UNSIGNED NOT NULL,
  `team_id` INT UNSIGNED NOT NULL,
  `pts` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `min` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`player_id`, `game_id`),
  INDEX `fk_game_id_idx` (`game_id` ASC),
  INDEX `fk_players_idx` (`player_id` ASC),
  INDEX `fk_team_id_idx` (`team_id` ASC),
  CONSTRAINT `fk_player__id`
    FOREIGN KEY (`player_id`)
    REFERENCES `mydb`.`players` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_game_id`
    FOREIGN KEY (`game_id`)
    REFERENCES `mydb`.`games` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_team_id`
    FOREIGN KEY (`team_id`)
    REFERENCES `mydb`.`teams` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
```

### B) How would you find files that begin with “0aH” and delete them given a folder (with subfolders)? Assume there are many files in the folder.

I’ll assume it is Linux machine and using what system offers could be good option for this task.

```bash
find /tmp -type f -name "0aH*" -delete
```

Deleting many files could affect/slow down system so batching deletes might be better.

```bash
find /tmp -type f -name "0aH*" > files; split -l 1000 files files.; for F in files.*; do cat $F | xargs rm -f; sleep 2; done; rm -rf files*
```

Maybe use crontab (e.g. run every n minutes or so) and delete unwanted files before number gets too big

```bash
find /tmp -type f -name "0aH*" | head -n 1000 | xargs rm -f
```

### C) Write a function that sorts 11 small numbers (<100) as fast as possible. Estimate how long it would take to execute that function 10 Billion (10^10) times on a normal machine?

This insertion sort function was first doing swap of elements, but I could gain some more speed by shifting elements inside array.

```php
function insertionSort(array $numbers): array
{
    for ($i = 1, $size = count($numbers); $i < $size; $i++) {
        $compareToValue = $numbers[$i];
        for ($j = $i; ($j > 0) && ($numbers[$j-1] > $compareToValue); $j—) {
            $numbers[$j] = $numbers[$j - 1];
        }
        $numbers[$j] = $compareToValue;
    }

    return $numbers;
}
```

So I tried to make estimate based on some runs. I generated array with some random numbers.
My assumption is that timing one call would put numbers a bit off so I executed function in sets of
10^1, 10^2 and up to 10^7 calls.

The results were:

```
Array to sort: 18, 75, 45, 48, 82, 79, 39, 9, 75, 72, 13
Executed          10 calls in   0.000035 seconds
Executed         100 calls in   0.000346 seconds
Executed        1000 calls in   0.003081 seconds
Executed       10000 calls in   0.029356 seconds
Executed      100000 calls in   0.274488 seconds
Executed     1000000 calls in   2.707066 seconds
Executed    10000000 calls in  27.489413 seconds
Executed 10000000000 calls in       7.64 hours [PREDICTION]
```

According to my measurements I guesstimate it would take around 7.5 hours.
But what about already sorted (or maybe almost sorted) arrays and arrays in reverse order?

So I timed also for “best case” - array with ascending numbers:

```
Array to sort: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
Executed          10 calls in   0.000013 seconds
Executed         100 calls in   0.000123 seconds
Executed        1000 calls in   0.000934 seconds
Executed       10000 calls in   0.010488 seconds
Executed      100000 calls in   0.115014 seconds
Executed     1000000 calls in   0.931912 seconds
Executed    10000000 calls in   9.458043 seconds
Executed 10000000000 calls in       2.63 hours [PREDICTION]
```

And then also timed for “worst case” - array with descending numbers:

```
Array to sort: 99, 98, 97, 96, 95, 94, 93, 92, 91, 90, 89
Executed          10 calls in   0.000081 seconds
Executed         100 calls in   0.000498 seconds
Executed        1000 calls in   0.005350 seconds
Executed       10000 calls in   0.041834 seconds
Executed      100000 calls in   0.402081 seconds
Executed     1000000 calls in   3.943409 seconds
Executed    10000000 calls in  47.510997 seconds
Executed 10000000000 calls in      13.20 hours [PREDICTION]
```

### D) Write a function that sorts 10000 powers (a^b) where a and b are random numbers between 100 and 10000? Estimate how long it would take on your machine?

```php
function mergeSort(array $numbers): array
{
    if (sizeof($numbers) <= 1) {
        return $numbers;
    }
    $splitToSize = (int)ceil(count($numbers) / 2);
    $left        = array_splice($numbers, 0, $splitToSize);

    return _merge(mergeSort($left), mergeSort($numbers));
}

function _merge(array $left, array $right): array
{
    $return = [];

    while (!empty($left) || !empty($right)) {
        if (empty($left) || empty($right)) {
            $return[] = (empty($left))
                ? array_shift($right) : array_shift($left);
        } else {
            $return[] = (bccomp(reset($left), reset($right)) < 0)
                ? array_shift($left) : array_shift($right);
        }
    }

    return $return;
}
```

Timings:
```
Sorted array with          10 elements in   0.001096 seconds
Sorted array with         100 elements in   0.023956 seconds
Sorted array with        1000 elements in   0.369834 seconds
```
Merge sort has complexity of O(n*log(n)) and based on this I set formula `(n * log2(n)) / ((n+1) * log2(n+1)) = t-of-n/t`

Using this formula for 1st and 2nd measurement
```
(10^1 * log2(10^1)) / (10^2 * log2(10^2)) = 0.001096/t
```
For 100 elements it would be t = 0.02192 (approximation), which kinda matches my timing.

Using this formula for 2nd and 3rd measurement
```
(10^2 * log2(10^2)) / (10^3 * log2(10^3)) = 0.023956/t
```
For 1000 elements it would be t = 0.35934 (approximation), which also kinda matches my timing.

Using this formula for 3rd and a case of having 10^4 measurement
```
(10^3 * log2(10^3)) / (10^4 * log2(10^4)) = 0.369834/t
```
This gives t = 4.93112 (approximation)

To sort sorts 10000 powers (a^b), where a and b are random numbers between 100 and 10000, it would take roughly 5 seconds. But to generate so many numbers it would take a lot more time and resources.
